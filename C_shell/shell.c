/*
Arttu Ristola
opnro. 0418747
8.1.2017
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#define LOGOUT 60
#define MAXNUM 40
#define MAXLEN 160

void sighandler(int sig)
{
	switch (sig) {
		case SIGALRM:
			printf("\nautologout\n");
			exit(0);
		default:
			break;
	}
	return;
}

int main(void)
{
	char * cmd, line[MAXLEN], * args[MAXNUM], * pm1[MAXNUM],  * pm2[MAXNUM];
	int background, i, j, k, argpos = 0, next = 0, haspipe = 0;
	pid_t pid, pid2, p_pid, wpid;
	int status;
	char *cwd[1024];
	char *hdir = getenv("HOME");// path for plain cd command
	int in, out;
	char input[64] = "", output[64] = "";
	char *word;
	signal(SIGALRM, sighandler);
	signal(SIGINT, sighandler);
	
	in = STDIN_FILENO;
	out = STDOUT_FILENO;


	while (1) {
	
		/* print the prompt */
		printf("$ ");
		//print current path
		if (getcwd(cwd, sizeof(cwd)) != NULL) {
			printf(cwd);
		}
		background = 0;
		printf(" > ");
		/* prompt printed */
		
		/* clearing pipe entries */
		memset(&pm1[0], 0, sizeof(pm1));
		memset(&pm2[0], 0, sizeof(pm2));

		/* read the users command */
		if (fgets(line,MAXLEN,stdin) == NULL) {
			printf("\nlogout\n");
			exit(0);
		}
		line[strlen(line) - 1] = '\0';
		
		if (strlen(line) == 0)
			continue;
		
		/* start to background? */
		if (line[strlen(line)-1] == '&') {
			line[strlen(line)-1]=0;
			background = 1;
		}



		/* split the command line to args */

		i = 0;
		j = 0;
		cmd = line;
				
		while ( (word = strtok(cmd, " ") ) != NULL) {

			//compare for '|'
			if(strcmp(word, "|") == 0) {
				haspipe = 1;
				//strncpy(pm1, args, MAXNUM);				
			}

			//compare for the less than sign
			else if(strcmp(word, "<") == 0) {
				in = -1;
				strcpy(input, args[i - 1]);			
			}
			else if(strcmp(word, ">") == 0)	{
				out = -1;
				next = 1; //grab the output file next		
			}			
			else {	
				
				if (next == 1) { //if we had > before don't put the string to arguments
					strcpy(output, word);
					next = 0;				
				}				
				else {
					args[i] = malloc(sizeof(word) + 1);
					strcpy(args[i], word);
					//printf("arg %d: %s\n", i, args[i]); //no need for args to show in prompt
					
					//create pipe arrays
					if (haspipe == 1) {
						pm2[j] = malloc(sizeof(word) + 1);
						strcpy(pm2[j], word);
						//printf("pm2 %d: %s\n", j, pm2[j]);//checkprint
						j++;
					}
					if (haspipe != 1) {
						pm1[i] = malloc(sizeof(word) + 1);
						strcpy(pm1[i], word);
						//printf("pm1 %d: %s\n", i, pm1[i]);//checkprint
					}
					i++;
				}
			}
			cmd = NULL;
			
		}
		argpos = i;
		if (strcmp(args[0], "exit") == 0) {
			exit(0);
		}

		/* cd: Moving between directories */
		if (strcmp(args[0], "cd") == 0) {
			if (getcwd(cwd, sizeof(cwd)) != NULL) { // get current dir to cwd
       				
				//moving next dir	
				if (args[1]) {
					//change dir 
					chdir(args[1]);
					args[1] = NULL; // must be cleared or may result to unwanted 'cd ..'	
				}

				//if no other arg is given 
				else if(args[1] == NULL) {
					//change to home dir 
					chdir(hdir);
				}
				else 
					perror("error with cd");

			}
			else
				perror("error with getcwd()");
			
			continue;
		}
		
		//prevents the empty argument.
		args[argpos] = NULL;

	
		/* fork to run the command */
		fflush(0);
							
	
		switch (p_pid = fork()) {
			case -1:
				/* error */ 
				perror("fork");
				continue;
			case 0:
				/* child process */
										
					/* pipe */
			
					if (haspipe){
	
						int fd[2];
						pipe(fd);

						if (pid = fork() == 0) {
							dup2(fd[1], 1);
  							close(fd[0]);
  							execvp(pm1[0], pm1);
							perror("execvp failure");
							exit(EXIT_SUCCESS);
						}
						if (pid2 = fork() == 0) {
  							dup2(fd[0], 0);
  							close(fd[1]);
  							execvp(pm2[0], pm2);
							perror("execvp failure");
							exit(EXIT_SUCCESS);
						}
						close(fd[0]);
						close(fd[1]);
						//wait until the child forks have stopped.						
						waitpid(pid, NULL, 0);							
						waitpid(pid2, NULL, 0);
						return(0);
						}


				// run redirections	
					if(input[0] != '\0') 
						in = open(input, O_RDONLY, 0);
        				
        				dup2(in, STDIN_FILENO); 

        				if (in != STDIN_FILENO)
						close(in);					
   				
				
    					if(output[0] != '\0')
						out = open(output, O_WRONLY|O_CREAT, 0666);
					
					dup2(out, STDOUT_FILENO);

					if (out != STDOUT_FILENO)
        					close(out);
    				
					//execute
					execvp(args[0], args);
					perror("execvp");
					exit(1);
		

			default:
				/* parent (shell) */
				if (!background) {
					waitpid(p_pid, NULL, 0);
					//while (wait(NULL)!=p_pid)
						//printf("some other child process exited\n");
				}
				break;
		
	
		}

		
		if (in > STDIN_FILENO)
			close(in);

		if (out > STDOUT_FILENO)
			close(out);

		while (argpos-- >= 0)
			free(args[argpos]);
		
		strcpy(input, "\0");
		strcpy(output, "\0");

		argpos = 0;
		if (haspipe)
			haspipe = 0;
	}

	return 0;
}

/* EOF */

