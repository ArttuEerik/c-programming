Creator: Arttu Ristola
Student no: 0418747

This program is part of my studies at Lappeenranta University of Technology.

This program is a simple Unix shell written in C. The shell can execute following commands with parameters:
$> cd [directory]
$> cd ..
$> cd (with no parameter navigates to home directory)
$> sleep [time]
$> ls [paramer(s)]
$> cat, less, grep, uname, wc, rm, ps (etc.)

Shell can run redirections with commands '<' and '>':
$> [command] > [file]
$> [command] < [file] > [target]

Shell can run commands with one(1) pipe with '|' command, as any other shell: 
$> [command] | [command]
Example:
$> ls -l | wc -l

Shell also prints your current directory in the prompt.

Any other functions are not guaranteed to work with this shell.
