/*
Ohjelma:.........Tasapainoittava binaaripuu
Tekijät:.........Kimmo Flykt (opnro.0438358), Arttu Ristola (opnro.0418747)
Päivämäärä:......13.11.2015
*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

/*************** Luodaan solmun rakennne *******************/
struct solmu { 
	int data;
	int korkeus;
	struct solmu *left;
	struct solmu *right;
};
typedef struct solmu node;

/********************** APUFUNKTIOITA ***********************/
/* Palauttaa suuremman arvon */
int max(int a, int b) { 
	return (a < b) ? b : a;
}
/* Palauttaa solmun korkeuden */
static int height(node *S) {
	return (S == NULL) ? 0 : S->korkeus; 
}
/* Laskee tasapainoarvon solmulle */
int getBalance(node *S) {
	if(S == NULL) {
		return 0;
	}
	return height(S->left) - height(S->right);
}

/***************************** KÄÄNNÖT ******************************/

static node* rotateLeft(node *juuri) {

	/* pointtereiden alustus */
	node *x = juuri->right;
	node *y = x->left;

	/* käännöt */
	x->left = juuri;
	juuri->right = y;

	/* korkeuden päivitys */
	juuri->korkeus = max(height(juuri->left), height(juuri->right)) + 1;
	x->korkeus = max(height(x->left), height(x->right)) + 1;
	return x;
}

static node* rotateRight(node *juuri) {

	/* pointtereiden alustus */
	node *x = juuri->left;
	node *y = x->right;

	/* käännöt */
	x->right = juuri;
	juuri->left = y;

	/* korkeuden päivitys */
	juuri->korkeus = max(height(juuri->left), height(juuri->right)) + 1;
	x->korkeus = max(height(x->left), height(x->right)) + 1;
	return x;
}

/************************** INSERT ***************************/
node *insert(node * leaf, node * parent, int key) {
	node *temp = NULL;

	if (leaf == NULL) { /* Uuden solmun luonti */
		temp = (node*) malloc(sizeof(node));
		temp->left = temp->right = NULL;		
		temp->data = key;
		temp->korkeus = 1;
		
		/* Tulostukset siitä, mihin lisättävät alkoit tallennetaan puussa */
		if (parent != NULL) {
			printf("%d tallentui alkion %d %s lapseksi\n", 
			key, parent->data, (key < parent->data)?"vasemmaksi":"oikeaksi");
		}		
		else printf("Alkio %d tallentui juureksi\n", key);
		return temp;
	}
	/* Tarkistus, onko lisättävä alkio jo puussa (uniikkipuu) */
	if (key == leaf->data) {
		printf("Alkio %d on jo puussa!\n", key);
		return leaf;
	}
	/* Sijoitus vasempaan haaraan */
	if (key < leaf->data) {
		leaf->left = insert(leaf->left, leaf, key);
	}
	/* Sijoitus oikeaan haaraan */
	else {
		leaf->right = insert(leaf->right, leaf, key);		
	}

	/*** Parent-alkion korjaus ja balance-arvon laskeminen ***/
	leaf->korkeus = max(height(leaf->left), height(leaf->right)) + 1;
	int balance = getBalance(leaf);

	/************* KÄÄNTÖKOMENNOT ***************/
	if (balance > 1 && key < leaf->left->data){ /* R kierto */
		printf("Suoritetaan RR kierto solmulle %d\n", leaf->data);
		return rotateRight(leaf);
	}
	if (balance < -1 && key > leaf->right->data){ /* L kierto */
		printf("Suoritetaan LL kierto solmulle %d\n", leaf->data);
		return rotateLeft(leaf);
	}
	if(balance > 1 && key > leaf->left->data) { /* LR kierto */
		printf("Suoritetaan LR kierto solmulle %d\n", leaf->data);
		leaf->left = rotateLeft(leaf->left);
		return rotateRight(leaf);
	}
	if(balance < -1 && key < leaf->right->data) { /* RL kierto */
		printf("Suoritetaan RL kierto solmulle %d\n", leaf->data);
		leaf->right = rotateRight(leaf->right);
		return rotateLeft(leaf);
	}
	return leaf;
}
/****************** TULOSTUKSET *********************/
/* Sivuttain puun tulostus */
void treeprint(node *root, node *parent, int counter){
	int i;
	if (root){
		counter++;
		
		treeprint(root->right, root, counter);
		if(parent == NULL){
			printf("%d\n", root->data);
		}
		else if(parent->right == root){
			for(i = 1; i < counter; i++){
				printf("    ");
			}
			printf("/%d\n", root->data);
		}
		else if(parent->left == root){
			for(i = 1; i < counter; i++){
				printf("    ");
			}
			printf("\\%d\n", root->data);
		}
		treeprint(root->left, root, counter);	
	}		
}
/* Lasketaan puun koko, käytetään samaa rakennetta kuin inorder */
int calculate(node *leaf, int counter){
	int i = 0;
	if(leaf){
		counter = counter + calculate(leaf->left, 0);
		counter = counter + calculate(leaf->right, 0);	
		i = 1;
	}
	return counter + i;
}
/* Inorder-tulostus, kun solmuja paljon */
void inorder(node *leaf){
	if(leaf){
		inorder(leaf->left);
		printf("%d\n", leaf->data);
		inorder(leaf->right);
	}
}

/************* ALKION HAKU JA TULOSTUS REITILLE, JOLLA ALKIOLLE PÄÄSTÄÄN ************/
node* search(node **leaf, int key) {

	if(!(*leaf)) { /* Ei alkiota puussa */
		printf("Haettua alkiota ei löytynyt!\n");
		return NULL;
	}
	if (key == (*leaf)->data) { /* Alkio löytyi */
		printf("Alkio %d\n", (*leaf)->data);
		return *leaf;
	}
	else if (key < (*leaf)->data) { /* Nykyisestä alkiosta jatketaan etsintää vasemmalle */
		printf("Alkio %d -> vasemmalle\n", (*leaf)->data);
		search(&((*leaf)->left), key);
	}
	else if (key > (*leaf)->data) { /* Nykyisestä alkiosta jatketaan etsintää oikealle */
		printf("Alkio %d -> oikealle\n", (*leaf)->data);
		search(&((*leaf)->right), key);
	}
 	
return NULL;
}

/**************** AVAINTEN LUKU TIEDOSTOSTA JA SIJOITUS PUUHUN **************/
node *haeAvaimet(node *root, node *temp, char *nimi) {
	int luku;
		
	FILE* tiedosto; /* Kahva tiedostoon */

	if ((tiedosto = fopen(nimi, "r")) == NULL) { /* Tiedoston avaus */
		perror("Luku epäonnistui! Tarkista tiedoston nimi!");
		exit(1);
	}
	/* Tiedoston luku ja sijoitus insert-metodilla */
	fscanf(tiedosto, "%d", &luku);
	while(!feof(tiedosto)) {
		root = insert(root, temp, luku);
		fscanf(tiedosto, "%d", &luku);	
	}
	/* Tiedoston sulku */
	fclose(tiedosto);
	return root;
}

/****************************** MAIN ALKAA ********************************/
int main(int argc, char *argv[]){ 
	
	/* Pointterit ja tarvittavat integerit */
	struct solmu *root = NULL;
	struct solmu *temp = NULL;
	int counter = 0;
	int i = 0;
	int valinta, avain;	
	
	/* Solmujen lisäys puuhun tiedostosta*/
 	root = haeAvaimet(root, temp, argv[1]);

	/******************** VALIKKO *************************/
	while(i == 0) {
		printf("\n");
		printf("VALIKKO\n");
		printf("1) Hae avain\n");
		printf("2) Lisää avain\n");
		printf("3) Tulosta puu\n");
		printf("0) Lopeta\n");	
		printf("Anna valinta: ");
		scanf("%d", &valinta);				
		switch(valinta) {
			case 1:
				printf("Anna haettava avain: ");
				scanf("%d", &avain);
				search(&root, avain);
				break;	
			case 2:
				printf("Anna lisättävä avain: ");
				scanf("%d", &avain);
				root = insert(root, temp, avain);
				break;
			case 3:
				if(calculate(root, 0) <= 100){
					printf("\nPuun tulostus\n");
					treeprint(root, temp, counter);
				}
				/* Jos puussa yli 100 solmua,
				tulostetaan puu Inorder-järjestyksessä pelkkinä lukuina */
				else {
					printf("\nPuu tulostetaan sisäjärjestyksessä\n");
					inorder(root);
				}
				break;	
			case 0:
				printf("Lopetetaan...\n");
				i = 1;
				break;
			default:
				printf("ERROR\n");
		}
	}
return 0;
}


/*eof*/
